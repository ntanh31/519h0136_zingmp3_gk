import 'package:flutter/material.dart';
import '../models/playlist.dart';
import '../models/quangcao.dart';
import '../models/theloai.dart';
import '../views/BaihatTrongAlbum.dart';
import '../views/BaihatTrongPlaylist.dart';
import '../views/BaihatTrongQuangcao.dart';
import '../views/BaihatTrongTheloai.dart';
import '../views/FullAlbum.dart';
import '../views/FullChuDe.dart';
import '../views/FullPlaylist.dart';
import '../views/HomeScreen.dart';
import '../views/TheloaiTheoChude.dart';
import '../views/login_field/login.dart';
import '../views/login_field/register.dart';
import '../views/login_field/start.dart';
import 'package:firebase_core/firebase_core.dart';
import 'models/album.dart';
import 'models/chude.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Color mainColor = Color(0xff7200a1);

  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        backgroundColor: mainColor
      ),
      home: StartScreen(),
      routes: <String,WidgetBuilder>{
        "Login" : (BuildContext context)=>Login(),
        "SignUp":(BuildContext context)=>Register(),
        "start":(BuildContext context)=>StartScreen(),
        "home":(BuildContext context)=>HomeScreen(),
        "fullAlbum":(BuildContext context)=>FullAlbum(),
        "fullPlaylist": (BuildContext context)=>FullPlaylist(),
        "fullChude": (BuildContext context)=>FullChuDe(),
        "baihattrongAlbum": (BuildContext context)=>BaihatTrongAlbum(Album()),
        "baihattrongPlaylist": (BuildContext context)=>BaihatTrongPlaylist(Playlist()),
        "theloaitheoChude": (BuildContext context)=>TheloaiTheoChuDe(Chude()),
        "baihattrongTheloai": (BuildContext context)=>BaihatTrongTheloai(Theloai()),
        "baihattrongQuangcao": (BuildContext context)=>BaihatTrongQuangcao(Quangcao()),
      },
    );
  }
}
