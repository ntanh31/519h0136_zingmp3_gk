import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';
import '../models/baihat.dart';

class MusicPlayer extends StatefulWidget {
  final Baihat? baihat;
  const MusicPlayer(this.baihat);
  @override
  MusicPlayerState createState() => MusicPlayerState();
}

class MusicPlayerState extends State<MusicPlayer> {
  bool playing = false;
  IconData playBtn = Icons.play_arrow;

  AudioPlayer? _player;
  AudioCache? cache;

  Duration position = new Duration();
  Duration musicLength = new Duration();

  Widget slider() {
    return Container(
      width: 300.0,
      child: Slider.adaptive(
          activeColor: Colors.white,
          inactiveColor: Colors.grey[350],
          value: position.inSeconds.toDouble(),
          max: musicLength.inSeconds.toDouble(),
          onChanged: (value) {
            seekToSec(value.toInt());
          }),
    );
  }

  //let's create the seek function that will allow us to go to a certain position of the music
  void seekToSec(int sec) {
    Duration newPos = Duration(seconds: sec);
    _player!.seek(newPos);
  }

  //Now let's initialize our player
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _player = AudioPlayer();
    cache = AudioCache(fixedPlayer: _player);

    //now let's handle the audioplayer time

    //this function will allow you to get the music duration
    _player!.onDurationChanged.listen((Duration duration) {
      setState(() {
        musicLength = duration;
      });
    });

    _player!.onAudioPositionChanged.listen((p) {
      setState(() {
        position = p;
      });
    });
  }

  @override
  Widget build(BuildContext context) {

    Color mainColor = Color(0xff7200a1);
    Color mainColor2 = Color(0xFF63008B);
    return SafeArea(
        child: Scaffold(
          backgroundColor: mainColor,
          //let's start by creating the main UI of the app
          body: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //Let's add some text title
                    Padding(
                      padding: const EdgeInsets.only(left: 12.0),
                      child: Text(
                        "${widget.baihat!.tenBaiHat}",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 38.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 12.0),
                      child: Text(
                        "${widget.baihat!.caSi}",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 24.0,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 24.0,
                    ),
                    //Let's add the music cover
                    Center(
                      child: Container(
                        width: 300.0,
                        height: 300.0,
                        child:  CircleAvatar(
                          backgroundImage: NetworkImage(widget.baihat!.hinhBaiHat as String),
                        ),
                      ),
                    ),

                    SizedBox(
                      height: 18.0,
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                          color: mainColor2,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30.0),
                            topRight: Radius.circular(30.0),
                          ),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              width: 500.0,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    "${position.inMinutes}:${position.inSeconds.remainder(60)}",
                                    style: TextStyle(
                                      fontSize: 18.0,
                                      color: Colors.white
                                    ),
                                  ),
                                  slider(),
                                  Text(
                                    "${musicLength.inMinutes}:${musicLength.inSeconds.remainder(60)}",
                                    style: TextStyle(
                                      fontSize: 18.0,
                                      color: Colors.white
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                IconButton(
                                  iconSize: 45.0,
                                  color: Colors.white,
                                  onPressed: () {},
                                  icon: Icon(
                                    Icons.skip_previous,
                                  ),
                                ),
                                IconButton(
                                  iconSize: 62.0,
                                  color: Colors.white,
                                  onPressed: () {
                                    //here we will add the functionality of the play button
                                    if (!playing) {
                                      //now let's play the song
                                      cache!.play(widget.baihat!.linkBaiHat as String);
                                      setState(() {
                                        playBtn = Icons.pause;
                                        playing = true;
                                      });
                                    } else {
                                      _player!.pause();
                                      setState(() {
                                        playBtn = Icons.play_arrow;
                                        playing = false;
                                      });
                                    }
                                  },
                                  icon: Icon(
                                    playBtn,
                                  ),
                                ),
                                IconButton(
                                  iconSize: 45.0,
                                  color: Colors.white,
                                  onPressed: () {},
                                  icon: Icon(
                                    Icons.skip_next,
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
