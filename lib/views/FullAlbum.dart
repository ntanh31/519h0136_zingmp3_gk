import 'package:flutter/material.dart';
import '../api/album-api.dart';
import '../models/album.dart';

class FullAlbum extends StatefulWidget {
  const FullAlbum({Key? key}) : super(key: key);

  @override
  State<FullAlbum> createState() => _FullAlbumState();
}

class _FullAlbumState extends State<FullAlbum> {
  @override
  Widget build(BuildContext context) {
    Color mainColor = Color(0xff7200a1);
    return SafeArea(
        child: Scaffold(
          backgroundColor: mainColor,
          appBar: AppBar(
            backgroundColor: mainColor,
            title: Text("Full Album"),
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () => Navigator.pushReplacementNamed(context, "home"),
            ),
          ),
          body: SingleChildScrollView(
            child: Container(
              child: FutureBuilder(
                future: fullAlbum(),
                builder: (context, AsyncSnapshot snapshot){
                  if(snapshot.hasData){
                    return Container(
                        height: 1500,
                        child: GridView.builder(
                          physics: PageScrollPhysics(),
                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount:2),
                          scrollDirection: Axis.vertical,
                          itemCount: snapshot.data.length,
                          itemBuilder: (BuildContext context, index){
                            Album album = snapshot.data[index];
                            return Card(
                              elevation: 0,
                              color: Colors.transparent,
                              child: InkWell(
                                onTap: (){
                                  Navigator.pushNamed(context, 'baihattrongAlbum', arguments: {'album': album});
                                },
                                child: Column(
                                  children: [
                                    Image.network(
                                      album.hinhAlbum!,
                                      width: 150,
                                      height: 150,
                                    ),
                                    Text("${album.tenAlbum}", style: TextStyle(fontSize: 17, color: Colors.white) ,overflow: TextOverflow.ellipsis,),
                                    Text("${album.tenCaSiAlbum}", style: TextStyle(fontSize: 15, color: Colors.white)),
                                  ],
                                ),
                              ),
                            );
                          },
                        )
                    );
                  }
                  return CircularProgressIndicator();
                },
              ),
            ),
          ),
        ));
  }
}
