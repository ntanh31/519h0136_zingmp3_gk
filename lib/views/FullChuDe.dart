import 'package:flutter/material.dart';
import '../api/chude-api.dart';

import '../models/chude.dart';

class FullChuDe extends StatefulWidget {
  const FullChuDe({Key? key}) : super(key: key);

  @override
  State<FullChuDe> createState() => _FullChuDeState();
}

class _FullChuDeState extends State<FullChuDe> {
  Color mainColor = Color(0xff7200a1);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: mainColor,
        appBar: AppBar(
          backgroundColor: mainColor,
          title: Text("Tất cả chủ đề"),
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.pushReplacementNamed(context, "home"),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            child: FutureBuilder(
              future: fullChuDe(),
              builder: (context, AsyncSnapshot snapshot){
                if(snapshot.hasData){
                  return Container(
                      height: 1500,
                      child: ListView.builder(
                        scrollDirection: Axis.vertical,
                        itemCount: snapshot.data.length,
                        itemBuilder: (BuildContext context, index){
                          Chude chude = snapshot.data[index];
                          return Card(
                            elevation: 0,
                            color: Colors.transparent,
                            child: InkWell(
                              child: Column(
                                children: [
                                  Image.network(
                                    chude.hinhChuDe!,
                                    width: 410,
                                    height: 119,
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      )
                  );
                }
                return CircularProgressIndicator();
              },
            ),
          ),
        ),
      ),
    );
  }
}
