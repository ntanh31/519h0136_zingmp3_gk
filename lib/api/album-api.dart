import '../models/album.dart';
import 'package:http/http.dart' as http;

Future<List<Album>> fetchAlbum() async{
  final response = await http.get(Uri.parse("https://anhnguyendb.000webhostapp.com/public_html/Server/albumhot.php"));
  return albumFromJson(response.body);
}

Future<List<Album>> fullAlbum() async{
  final response = await http.get(Uri.parse("https://anhnguyendb.000webhostapp.com/public_html/Server/tatcaalbum.php"));
  return albumFromJson(response.body);
}

