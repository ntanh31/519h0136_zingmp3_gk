import '../models/playlist.dart';
import 'package:http/http.dart' as http;

Future<List<Playlist>> fetchPlaylist() async{
  final response = await http.get(Uri.parse("https://anhnguyendb.000webhostapp.com/public_html/Server/playlistforcurrentday.php"));
  return playlistFromJson(response.body);
}

Future<List<Playlist>> fullPlaylist() async{
  final response = await http.get(Uri.parse("https://anhnguyendb.000webhostapp.com/public_html/Server/danhsachcacplaylist.php"));
  return playlistFromJson(response.body);
}