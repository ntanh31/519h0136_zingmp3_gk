import 'dart:convert';

List<Baihat> baihatFromJson(String str) => List<Baihat>.from(json.decode(str).map((x) => Baihat.fromJson(x)));

String baihatToJson(List<Baihat> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Baihat {
  String? idBaiHat;
  String? tenBaiHat;
  String? hinhBaiHat;
  String? caSi;
  String? linkBaiHat;
  String? luotThich;

  Baihat({
    this.idBaiHat,
    this.tenBaiHat,
    this.hinhBaiHat,
    this.caSi,
    this.linkBaiHat,
    this.luotThich,
  });

  factory Baihat.fromJson(Map<String, dynamic> json) => Baihat(
    idBaiHat: json["IdBaiHat"],
    tenBaiHat: json["TenBaiHat"],
    hinhBaiHat: json["HinhBaiHat"],
    caSi: json["CaSi"],
    linkBaiHat: json["LinkBaiHat"],
    luotThich: json["LuotThich"],
  );

  Map<String, dynamic> toJson() => {
    "IdBaiHat": idBaiHat,
    "TenBaiHat": tenBaiHat,
    "HinhBaiHat": hinhBaiHat,
    "CaSi": caSi,
    "LinkBaiHat": linkBaiHat,
    "LuotThich": luotThich,
  };
}
